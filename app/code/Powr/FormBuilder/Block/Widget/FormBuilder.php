<?php
namespace Powr\FormBuilder\Block\Widget;

class FormBuilder extends \Magento\Framework\View\Element\Template implements \Magento\Widget\Block\BlockInterface
{
    protected $_template = "formbuilder/widget.phtml";
}