<?php

namespace Powr\FormBuilder\Plugin\Wysiwyg;

use Magento\Cms\Model\Wysiwyg\Config as Subject;
use Magento\Framework\DataObject;
use Powr\FormBuilder\Model\Wysiwyg\Powr;

class ConfigPlugin
{
    /**
     * @var Powr
     */
    private $powrform;

    /**
     * ConfigPlugin constructor.
     * @param Powr $powr
     */
    public function __construct(
        Powr $powrform
    ) {
        $this->powrform = $powrform;
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetConfig(Subject $subject, DataObject $config) // DataObject
    {
        $powrformPluginSettings = $this->powrform->getPluginSettings($config);
        $config->addData($powrformPluginSettings);
        return $config;
    }
}
